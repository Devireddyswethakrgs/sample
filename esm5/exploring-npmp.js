import { Component, NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

var HeaderComponent = /** @class */ (function () {
    function HeaderComponent() {
    }
    HeaderComponent.prototype.ngOnInit = function () {
    };
    return HeaderComponent;
}());
HeaderComponent.decorators = [
    { type: Component, args: [{
                selector: 'app-header',
                template: "<p>\n  header works here krgsswetha k.\n</p>\n",
                styles: [""]
            },] },
];
HeaderComponent.ctorParameters = function () { return []; };
var HeaderModule = /** @class */ (function () {
    function HeaderModule() {
    }
    return HeaderModule;
}());
HeaderModule.decorators = [
    { type: NgModule, args: [{
                declarations: [
                    HeaderComponent
                ],
                imports: [
                    BrowserModule
                ],
                exports: [
                    HeaderComponent
                ]
            },] },
];

export { HeaderModule, HeaderComponent as ɵa };
//# sourceMappingURL=exploring-npmp.js.map
